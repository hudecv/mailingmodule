<?php

declare(strict_types=1);

namespace BlamelessWeb;

use Nette;
use Nette\Schema\Expect;

use BlamelessWeb\Mailing\Services;

class MailingExtension extends Nette\DI\CompilerExtension
{
    public function getConfigSchema(): Nette\Schema\Schema
    {
        return Expect::structure([
            'contact' => Expect::structure([
                'name' => Expect::string(),
                'to' => Expect::string(),
                'from' => Expect::string(),
                'subject' => Expect::string(),
                'bcc' => Expect::string()->default("")
            ])
        ]);
    }

    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();
        $builder
            ->addDefinition($this->prefix('emailService'))
            ->setFactory(Services\EmailService::class, [$this->config]);
    }
}