<?php

declare(strict_types=1);

namespace BlamelessWeb\Mailing\Services;

use Nette\Mail;

final class EmailService
{
	/** @var object */
	private $config;

	public function __construct(object $config)
	{
		$this->config = $config;
	}

	public function sendContactEmail($contact)
	{
		$cfgContact = $this->config->contact;
		
		$bccList = explode(';', $cfgContact->bcc);

		$mail = new Mail\Message;
		$mail
			->addTo($cfgContact->to)
			->setFrom($cfgContact->from, $cfgContact->name)
			->addReplyTo($contact->email, $contact->name)
			->setSubject($cfgContact->subject)
			->setBody($contact->message);

		array_map(function($bcc) use ($mail) { $mail->addBcc($bcc); }, $bccList);

		$mailer = new Mail\SendmailMailer;
		$mailer->send($mail);
	}

	public function getConfig()
	{
		return $this->config;
	}
}